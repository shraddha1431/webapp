# -*- coding: utf-8 -*-
"""Main Controller"""

from tg import expose, flash, require, url, lurl
from tg import request, redirect,session, tmpl_context
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.exceptions import HTTPFound
from webapp.lib.base import BaseController
from webapp.controllers.error import ErrorController
from webapp.lib.app_globals import Globals
from beaker.session import Session

__all__ = ['RootController']


class RootController(BaseController):
    """
    The root controller for the webapp application.

    All the other controllers and WSGI applications should be mounted on this
    controller. For example::

        panel = ControlPanelController()
        another_app = AnotherWSGIApplication()

    Keep in mind that WSGI applications shouldn't be mounted directly: They
    must be wrapped around with :class:`tg.controllers.WSGIAppController`.

    """

    error = ErrorController()
    

    def _before(self, *args, **kw):
        tmpl_context.project_name = "webapp"

    @expose('index.jinja')
    def index(self):
        """Handle the front-page."""
        return dict(page='index')

    @expose('info.jinja')
    def info(self):
        return dict(page='info')
    
    @expose('existingorg.jinja')
    def existingorg(self):
        orgs = Globals.sp.getOrganisationNames()
        return dict(orgs = orgs)
    
    @expose('neworg.jinja')
    def neworg(self):
        return dict(page='neworg')
    
    @expose('orgdetail.jinja')
    def orgdetail(self, **kw):
        orgParams = []
        orgParams.append(kw['orgname'])
        orgParams.append(kw['from_day']+"-"+kw['from_month']+"-"+kw['from_year'])
        orgParams.append(kw['to_day']+"-"+kw['to_month']+"-"+kw['to_year'])
        orgParams.append(kw['orgtype'])
        return dict(params=orgParams)
    
    @expose('json')
    def getFinancialYears(self, **kw):
        print kw
        financialyears = Globals.sp.getFinancialYear(kw['orgname'])
        return dict(years= financialyears)
    
    @expose('login.jinja')
    def login(self, **kw):
        params=[kw['orgname'],kw['fromDate'],kw['toDate'],kw['orgtype']]
        deploy=Globals.sp.Deploy(params)
        session['gnukhata']=deploy[1]
        session['orgname']=params[0]
        session['financialFrom']=params[1]
        session['financialTo']=params[2]
        session['orgtype']=params[3]
        return dict(page='login')
    
    @expose('login.jinja')
    def loginAfterExistingForm(self, **kw):
        orgname=kw['orgname']
        year = kw['financialYear']
        Year = year.split(",")
        params = [orgname,Year[0],Year[1]]
        client_id = Globals.sp.getConnection(params)
        session['gnukhata']=client_id
        session['orgname']=orgname
        session['financialFrom']=Year[0]
        session['financialTo']=Year[1]
        isngo=Globals.sp.organisation.getOrganizationType(client_id)
        print isngo
        if(isngo):
            session['orgtype']="ngo"
        else:
            session['orgtype']="profit making"
        print client_id
        return dict(id=client_id)
